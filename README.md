# Structure and Interpretation of Computer Programs

* [structure and interpretation of computer programs
  ](https://www.google.com/search?q=structure+and+interpretation+of+computer+programs
* [SICP Web Site](https://mitpress.mit.edu/sites/default/files/sicp/index.html)
* [*Structure and Interpretation of Computer Programs*
  ](https://web.mit.edu/alexmv/6.037/sicp.pdf)
* [SICP](https://sicpebook.wordpress.com/)